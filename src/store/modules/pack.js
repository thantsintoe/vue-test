const state = {
  packList: null,
  selectedPack: null
}

const mutations = {
  SET_PACK_LIST (state, payload) {
    state.packList = payload
  },
  SELECT_PACK (state, payload) {
    state.selectedPack = payload
  }
}

const actions = {
  setPackList: ({ commit }, payload) => {
    commit('SET_PACK_LIST', payload)
  },
  selectPack: ({ commit }, payload) => {
    commit('SELECT_PACK', payload)
  }
}

const getters = {
  getPackList: state => {
    return state.packList
  },
  getSelectedPack: state => {
    return state.selectedPack
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
