const state = {
  account: null
}

const mutations = {
  ADD_ACCOUNT (state, payload) {
    state.account = payload
  },
  REMOVE_ACCOUNT (state) {
    state.account = null
  }
}

const actions = {
  addAccount: ({ commit }, payload) => {
    commit('ADD_ACCOUNT', payload)
  },
  removeAccount: ({ commit }) => {
    commit('REMOVE_ACCOUNT')
  }
}

const getters = {
  getAccount: state => {
    return state.account
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
