import Vue from 'vue'
import Vuex from 'vuex'
import account from './modules/account'
import pack from './modules/pack'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    account,
    pack
  }
})
